var Request = function () {

};

$.extend(Request.prototype, {

    createInvoiceRow: function (invoice_id, token) {
        var self = this;
        $.post( "/ajax/createInvoiceRow", { invoice_id: invoice_id, _token: token })
            .done(function( data ) {
                var json = JSON.parse(data);
                self.addInvoiceRowHtml(json.id);
            });
    },

    deleteInvoiceRow: function (el, invoice_row_id, token) {

        var conf = confirm('Weet u zeker dat u deze factuur regel wilt verwijderen?');

        if(conf) {
            $.post( "/ajax/deleteInvoiceRow", { invoice_row_id: invoice_row_id, _token: token })
                .done(function(  ) {
                    $('#row_' + invoice_row_id).remove();
                });
        }
    },
    
    addInvoiceRowHtml: function (id) {
        var clone = $('#copy_invoice_row').clone(true);
        var html = clone.html().split('<id>').join(id);

        $('#invoice_rows').append(html);
    },

    saveValue: function (el_id, object, id, key, token, modifyValue) {
        var val = $('#' + el_id).val();

        if(modifyValue && modifyValue == 'double') {
            val = parseFloat(val.split(',').join('.'));
        }

        $.post( "/ajax/saveValue",
            {
                object: object,
                id: id,
                key: key,
                value: val,
                _token: token
            })
            .done(function( ) {
                $('#' + el_id).addClass('saved');

                setTimeout(function () {
                    if($('#' + el_id).hasClass( "saved" )) {
                        $('#' + el_id).removeClass('saved');
                    }
                }, 2000);

            });
    },

});

var oRequest = new Request();


$(function () {

    $( ".numbersAndComma" ).on('input', function() {
        var value=$(this).val().replace(/[^0-9.,]*/g, '');
        value=value.replace(/\.{2,}/g, '.');
        value=value.replace(/\.,/g, ',');
        value=value.replace(/\,\./g, ',');
        value=value.replace(/\,{2,}/g, ',');
        value=value.replace(/\.[0-9]+\./g, '.');
        $(this).val(value)

    });

});

