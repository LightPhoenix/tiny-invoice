<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\AjaxController;

//Auth::routes();

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
if (config('invoice.register'))
{
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register');
}
else
{
    Route::match(['get','post'], 'register', function () {
        return view('errors.403');
    })->name('register');
}

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');


/* Client */
Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

Route::get('/client', ['as' => 'client.index', 'uses' => 'ClientController@index']);

Route::get('/client/add', ['as' => 'client.add', 'uses' => 'ClientController@addEdit']);
Route::post('/client/add', ['as' => 'client.add', 'uses' => 'ClientController@addEditPost']);
Route::get('/client/{id}/edit', ['as' => 'client.edit', 'uses' => 'ClientController@addEdit']);

Route::get('/client/{id}/delete', ['as' => 'client.delete', 'uses' => 'ClientController@deleteClient']);

/* Invoice */
Route::get('/invoice', ['as' => 'invoice.index', 'uses' => 'InvoiceController@index']);
Route::get('/invoice/add', ['as' => 'invoice.add', 'uses' => 'InvoiceController@addEdit']);
Route::post('/invoice/add', ['as' => 'invoice.add', 'uses' => 'InvoiceController@addEditPost']);
Route::get('/invoice/{id}/edit', ['as' => 'invoice.edit', 'uses' => 'InvoiceController@addEdit']);

Route::get('/invoice/{id}/delete', ['as' => 'invoice.delete', 'uses' => 'InvoiceController@deleteInvoice']);

Route::post('/invoice/pay', ['as' => 'pay.post', 'uses' => 'PaymentController@postPayment']);
Route::get('/invoice/{hash}/pay', ['as' => 'pay.index', 'uses' => 'PaymentController@index']);
Route::any('/invoice/{hash}/payed', ['as' => 'pay.payed', 'uses' => 'PaymentController@payed']);

Route::get('/invoice/{hash}/pdf', ['as' => 'invoice.pdf', 'uses' => 'InvoiceController@pdf']);


/* AJAX */
$oAjax = new AjaxController();
$oAjax->buildRoutes();