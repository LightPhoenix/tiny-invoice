@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Klanten
                        <a href="{{ route('client.add') }}" class="btn btn-success btn-add">Nieuwe klant</a></div>

                    <div class="panel-body">

                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Bedrijfsnaam</th>
                                    <th scope="col">Naam</th>
                                    <th scope="col">Adres</th>
                                    <th scope="col">E-mail</th>
                                    <th scope="col">Telefoon</th>
                                    <th scope="col">Telefoon 2</th>
                                    <th scope="col">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($aData as $row)
                                    <tr>
                                        <th scope="row">{{ $row->id }}</th>
                                        <td>{{ $row->companyname }}</td>
                                        <td>{{ $row->firstname . ' ' . $row->lastname }}</td>
                                        <td>{{ $row->street . ', ' . $row->zip . ' ' . $row->city }}</td>
                                        <td>{{ $row->email }}</td>
                                        <td>{{ $row->phone_1 }}</td>
                                        <td>{{ $row->phone_2 }}</td>
                                        <td>
                                            <a class="actionIcon first" href="{{ route('client.edit', $row->id) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                            <a class="actionIcon" href="{{ route('client.delete', $row->id) }}" onclick="return confirm('Weet u zeker dat u deze klant wilt verwijderen?');"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="center">
        {{ $aData->links() }}
    </div>
@endsection
