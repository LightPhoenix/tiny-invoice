@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    @if($id > 0)
                        <div class="panel-heading">Klant bewerken</div>
                    @else
                        <div class="panel-heading">Klant toevoegen</div>
                    @endif

                    <div class="panel-body">

                        {{ Form::open(['route' => 'client.add']) }}

                            {!! Form::hidden('id', $id) !!}

                            <div class="form-group">
                                <h4>Bedrijfsinformatie</h4>
                            </div>

                            <div class="form-group">
                                {!! Form::label('companyname', 'Bedrijfsnaam') !!}
                                {!! Form::text('companyname', $oController->inputOrObject('companyname', $oClient), ['class' => 'form-control']) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('firstname', 'Voornaam') !!}
                                {!! Form::text('firstname', $oController->inputOrObject('firstname', $oClient), ['class' => 'form-control']) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('lastname', 'Achternaam') !!}
                                {!! Form::text('lastname', $oController->inputOrObject('lastname', $oClient), ['class' => 'form-control']) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('invoice_to', 'Factuur Tav.') !!}
                                {!! Form::text('invoice_to', $oController->inputOrObject('invoice_to', $oClient), ['class' => 'form-control']) !!}
                            </div>

                            <div class="form-group">
                                <h4>Adresgegevens</h4>
                            </div>

                            <div class="form-group">
                                {!! Form::label('street', 'Straat') !!}
                                {!! Form::text('street', $oController->inputOrObject('street', $oClient), ['class' => 'form-control']) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('zip', 'Postcode') !!}
                                {!! Form::text('zip', $oController->inputOrObject('zip', $oClient), ['class' => 'form-control']) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('city', 'Stad') !!}
                                {!! Form::text('city', $oController->inputOrObject('city', $oClient), ['class' => 'form-control']) !!}
                            </div>

                            <div class="form-group">
                                <h4>Contactgegevens</h4>
                            </div>

                        <div class="form-group">
                            {!! Form::label('email', 'E-mail') !!}
                            {!! Form::text('email', $oController->inputOrObject('email', $oClient), ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('phone_1', 'Telefoon 1') !!}
                            {!! Form::text('phone_1', $oController->inputOrObject('phone_1', $oClient), ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('phone_2', 'Telefoon 2') !!}
                            {!! Form::text('phone_2', $oController->inputOrObject('phone_2', $oClient), ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::submit('Opslaan', ['class' => 'btn btn-success']) !!}
                        </div>

                        {{ Form::close() }}


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection