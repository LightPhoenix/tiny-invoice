@extends('layouts.payment')

@section('content')

    <div class="paymentBox">

        {{ Form::open(['route' => 'pay.post']) }}

        {!! Form::hidden('hash', $hash) !!}

        <div class="topBar">
            <h1>Factuur {{ date('Y', strtotime($oInvoice->date)) }}-{{ $oInvoice->id }} <span class="right">&euro; {{ number_format($oInvoice->getAmount(), 2, ',', '.') }}</span></h1>
            <h2>{{$oInvoice->subject}}</h2>
        </div>
        <div class="content">
            @if($oInvoice->payed == null)

                @if($oInvoice->payment_status != null && $oInvoice->payment_status != 'paid')
                    <b>Uw betaling is niet geslaagd, probeer het nog eens.</b><br><br>
                @endif

                Betaal uw factuur eenvoudig en snel met iDeal.<br><br>
                Factuur nummer: {{ date('Y', strtotime($oInvoice->date)) }}-{{ $oInvoice->id }}<br>
                Factuur datum: {{ date('d-m-Y', strtotime($oInvoice->date)) }}<br>
                Betalen per: {{ date('d-m-Y', strtotime($oInvoice->date_due)) }}<br><br>

                {!! Form::select('issuer_id', $aIssuers, null, ['class' => 'form-control']) !!}
            @else
                Uw betaling is geslaagd!<br><br>

                Betaald op: {{ date('d-m-Y H:i:s', strtotime($oInvoice->payed)) }}
            @endif
        </div>

        @if($oInvoice->payed == null)
            {!! Form::submit('Betalen', ['class' => 'payButton']) !!}
        @endif

        {{ Form::close() }}

    </div>

@endsection