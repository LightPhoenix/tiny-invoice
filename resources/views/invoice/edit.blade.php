@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    @if($id > 0)
                        <div class="panel-heading">Factuur bewerken</div>
                    @else
                        <div class="panel-heading">Factuur toevoegen</div>
                    @endif

                    <div class="panel-body">

                        {{ Form::open(['route' => 'invoice.add']) }}

                        {!! Form::hidden('id', $id) !!}

                        <div class="form-group">
                            {!! Form::label('subject', 'Onderwerp') !!}
                            {!! Form::text('subject', $oController->inputOrObject('subject', $oInvoice), ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('date', 'Datum') !!}
                            <? $sDate = ($oController->inputOrObject('date', $oInvoice)) ? date('d-m-Y', strtotime($oController->inputOrObject('date', $oInvoice))) : date('d-m-Y') ; ?>
                            {!! Form::text('date', $sDate, ['class' => 'form-control datepicker']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('date_due', 'Uitelijke betaaldatum') !!}
                            <? $sDateDue = ($oController->inputOrObject('date_due', $oInvoice)) ? date('d-m-Y', strtotime($oController->inputOrObject('date_due', $oInvoice))) : date("d-m-Y", strtotime("+2 week")); ; ?>
                            {!! Form::text('date_due', $sDateDue, ['class' => 'form-control datepicker']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('payed', 'Betaald op') !!}
                            <? $sPayed = ($oController->inputOrObject('payed', $oInvoice)) ? date('d-m-Y', strtotime($oController->inputOrObject('payed', $oInvoice))) : null ; ?>
                            {!! Form::text('payed', $sPayed, ['class' => 'form-control datepicker']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('client_id', 'Klant') !!}
                            {!! Form::select('client_id', $aClientList, $oController->inputOrObject('client_id', $oInvoice), ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::submit('Opslaan', ['class' => 'btn btn-success']) !!}
                            @if($id > 0)
                                <a class="btn btn-default" target="_blank" href="{{ route('invoice.pdf', $oInvoice->hash) }}"><i class="fa fa-cloud-download"></i></a>
                            @endif
                        </div>

                        {{ Form::close() }}


                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">

                    <div class="panel-heading">Factuur regels</div>

                    <div class="panel-body">

                        @if($id > 0)
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                            <div id="invoice_rows">


                                <div class="row">
                                    <div class="col-md-2 noPadRight">
                                        {!! Form::label('amount', 'Aantal') !!}
                                    </div>
                                    <div class="col-md-5 noPadRight">
                                        {!! Form::label('subject', 'Onderwerp') !!}
                                    </div>
                                    <div class="col-md-2 noPadRight">
                                        {!! Form::label('vat', 'BTW') !!}
                                    </div>
                                    <div class="col-md-2 noPadRight">
                                        {!! Form::label('amount', 'Prijs (exc. Btw)') !!}
                                    </div>
                                    <div class="col-md-1 noPadRight">
                                        &nbsp;
                                    </div>
                                </div>

                                @foreach($aInvoiceRows as $row)
                                    <div class="row invoiceRow" id="row_{{$row->id}}">
                                        <div class="col-md-2 noPadRight">
                                            {!! Form::select('amount_' . $row->id , $aAmountList, $row->amount, ['id' => 'amount_' . $row->id, 'class' => 'form-control',
                                            'onchange' => "oRequest.saveValue(this.id, 'InvoiceRow', '" . $row->id . "', 'amount', $('#token').val() )" ]) !!}
                                        </div>
                                        <div class="col-md-5 noPadRight">
                                            {!! Form::text('subject_' . $row->id, $row->subject, ['id' => 'subject_' . $row->id, 'placeholder' => 'Onderwerp', 'class' => 'form-control',
                                            'onblur' => "oRequest.saveValue(this.id, 'InvoiceRow', '" . $row->id . "', 'subject', $('#token').val() )"]) !!}
                                        </div>
                                        <div class="col-md-2 noPadRight">
                                            {!! Form::select('vat_' . $row->id, $aVatList, $row->vat, ['id' => 'vat_' . $row->id, 'class' => 'form-control',
                                            'onchange' => "oRequest.saveValue(this.id, 'InvoiceRow', '" . $row->id . "', 'vat', $('#token').val() )"]) !!}
                                        </div>
                                        <div class="col-md-2 noPadRight">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="priceAddon_{{$row->id}}">&euro;</span>
                                            {!! Form::text('price_' . $row->id, number_format((double)$row->price, 2, ',', '.'),
                                                [
                                                    'id' => 'price_' . $row->id,
                                                    'aria-describedby' => 'priceAddon_' . $row->id,
                                                    'class' => 'form-control numbersAndComma',
                                                    'onblur' => "oRequest.saveValue(this.id, 'InvoiceRow', '" . $row->id . "', 'price', $('#token').val(), 'double' )"
                                                ]) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-1 noPadRight">
                                            <a class="fa fa-trash-o btn btn-danger deleteBtn" onclick="oRequest.deleteInvoiceRow(this, {{$row->id}}, $('#token').val())" aria-hidden="true"></a>
                                        </div>
                                    </div>
                                @endforeach

                            </div>

                            <div class="invoiceRow">
                                <button class="btn btn-primary" onclick="oRequest.createInvoiceRow({{ $id }}, $('#token').val())"><i class="fa fa-plus-circle" aria-hidden="true"></i> Nieuwe regel</button>
                            </div>

                        @else
                            U dient de factuur eerst op te slaan voordat u factuur regels kunt toevoegen.
                        @endif

                        <div style="display: none;" id="copy_invoice_row">
                            <div class="row invoiceRow" id="row_<id>">
                                <div class="col-md-2 noPadRight">
                                    {!! Form::select('amount_<id>' , $aAmountList, '', ['id' => 'amount_<id>', 'class' => 'form-control',
                                            'onchange' => "oRequest.saveValue(this.id, 'InvoiceRow', '<id>', 'amount', $('#token').val() )"]) !!}
                                </div>
                                <div class="col-md-5 noPadRight">
                                    {!! Form::text('subject_<id>', '', ['id' => 'subject_<id>', 'placeholder' => 'Onderwerp', 'class' => 'form-control',
                                            'onchange' => "oRequest.saveValue(this.id, 'InvoiceRow', '<id>', 'subject', $('#token').val() )"]) !!}
                                </div>
                                <div class="col-md-2 noPadRight">
                                    {!! Form::select('vat_<id>', $aVatList, null, ['id' => 'vat_<id>', 'class' => 'form-control',
                                            'onchange' => "oRequest.saveValue(this.id, 'InvoiceRow', '<id>', 'vat', $('#token').val() )"]) !!}
                                </div>
                                <div class="col-md-2 noPadRight">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="priceAddon_<id>">&euro;</span>
                                        {!! Form::text('price_<id>', '0,00',
                                            [
                                                'id' => 'price_<id>',
                                                'class' => 'form-control numbersAndComma',
                                                'aria-describedby' => 'priceAddon_<id>',
                                                'onchange' => "oRequest.saveValue(this.id, 'InvoiceRow', '<id>', 'price', $('#token').val() )"
                                            ]) !!}
                                    </div>
                                </div>
                                <div class="col-md-1 noPadRight">
                                    <a class="fa fa-trash-o btn btn-danger deleteBtn" onclick="oRequest.deleteInvoiceRow(this, <id>, $('#token').val())" aria-hidden="true"></a>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function () {
            $('.datepicker').datepicker({format: 'dd-mm-yyyy', locale: 'nl'});
        });
    </script>
@endsection