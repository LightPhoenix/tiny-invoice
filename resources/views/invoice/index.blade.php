@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Facturen
                        <a href="{{ route('invoice.add') }}" class="btn btn-success btn-add">Nieuwe factuur</a></div>

                    <div class="panel-body">

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Datum</th>
                                <th scope="col">Onderwerp</th>
                                <th scope="col">Klant</th>
                                <th scope="col">Betalen per</th>
                                <th scope="col">Bedrag (inc. Btw)</th>
                                <th scope="col">Betaald op</th>
                                <th scope="col">&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($aData as $row)
                                <tr>
                                    <th scope="row">{{ date('Y', strtotime($row->date)) }}-{{ $row->id }}</th>
                                    <td>{{ date('d-m-Y', strtotime($row->date)) }}</td>
                                    <td>{{ $row->subject }}</td>
                                    <td>{{ $row->client()->companyname }}</td>
                                    <td>{{ date('d-m-Y', strtotime($row->date_due)) }}</td>
                                    <td>&euro; {{ number_format($row->getAmount(), 2, ',', '.') }}</td>
                                    @if($row->payed == null)
                                        <td>&nbsp;</td>
                                    @else
                                        <td>{{ date('d-m-Y H:i:s', strtotime($row->payed)) }}</td>
                                    @endif
                                    <td width="13%">

                                        <a class="actionIcon first" href="{{ route('pay.index', $row->hash) }}" target="_blank"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
                                        {{--<a class="actionIcon first" href=""><i class="fa fa-share" aria-hidden="true"></i></a>--}}
                                        <a class="actionIcon" href="{{ route('invoice.pdf', $row->hash) }}" target="_blank"><i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                                        <span class="separator">|</span>
                                        <a class="actionIcon first" href="{{ route('invoice.edit', $row->id) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a class="actionIcon" href="{{ route('invoice.delete', $row->id) }}" onclick="return confirm('Weet u zeker dat u deze klant wilt verwijderen?');"><i class="fa fa-trash" aria-hidden="true"></i></a>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="center">
        {{ $aData->links() }}
    </div>
@endsection
