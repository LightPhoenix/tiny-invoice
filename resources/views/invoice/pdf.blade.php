<style type="text/css">

    .invoice {
        font-family: 'Raleway', sans-serif;
    }

    .header {
        font-size: 14px;
        margin-top: 110px;
    }

    .right {
        text-align: right;
    }

    .logo {
        float: right;
    }

    .rowsHeader th, .priceBox {
        border-top: 0.5px solid black;
        border-bottom: 0.5px solid black;
        padding-top: 5px;
        padding-bottom: 6px;
    }

    .priceBox {
        font-size: 14px;
        border-top: none;
    }

    .priceBox.total {
        border: none;
        padding-top: 3px;
    }

    .productTable {
        border-spacing: 0px;
        font-size: 14px;
    }

    .productRow td {
        padding-top: 5px;
        padding-bottom: 5px;
    }

    h1 {
        margin-top: 0;
    }

    .footer{
        position: absolute;
    }

    .footer{
        bottom: 0;
        text-align: center;
        color: gray;
        width: 100%;
        font-family: 'Raleway', sans-serif;
        font-size: 12px;
    }

    .invoice-information {
        font-size: 14px;
    }

    .price {
        margin-right: -8px;
    }
</style>

<div class="invoice">

    <img class="logo" src="images/pdf/logo.png" width="50%">

    <table class="header" width="100%">
        <tr>
            <td width="50%">
                <h1>Factuur</h1>
                <div class="client-information">
                    {{ $data->client()->companyname }} @if($data->client()->invoice_to) ({{ $data->client()->invoice_to }}) @endif<br>
                    {{ $data->client()->street }}<br>
                    {{ $data->client()->zip }} {{ $data->client()->city }}<br><br>
                </div>
                <div class="invoice-information">
                    <b>Factuurnummer:</b> {{ date('Y', strtotime($data->date)) }}-{{ $data->id }}<br>
                    <b>Factuurdatum:</b> {{ date('d-m-Y', strtotime($data->date)) }}<br><br>
                    <b>Betreft:</b> {{$data->subject}}
                </div>
            </td>
            <td width="50%" class="right">
                {{ config('invoice.address') }}<br>
                {{ config('invoice.zip') }} {{ config('invoice.city') }}<br><br>
                {{ config('invoice.company_nr') }}<br>
                {{ config('invoice.vat_nr') }}<br>
                {{ config('invoice.account') }}<br>
                {{ config('invoice.account_holder') }}<br><br>
                {{ config('invoice.email') }}<br>
                {{ config('invoice.phone') }}<br>
                {{ config('invoice.website') }}<br>
            </td>
        </tr>
    </table>

    <div class="rows">
        <br><br>
        <table width="100%" class="productTable">
            <thead>
                <tr class="rowsHeader">
                    <th width="10%">Aantal</th>
                    <th width="45%">Onderdeel</th>
                    <th width="5%">Btw</th>
                    <th width="15%" class="right">Per stuk</th>
                    <th width="15%" class="right">Totaal</th>
                </tr>
            </thead>
            <tbody>
            @foreach($data->rows() as $row)
                <tr class="productRow">
                    <td>{{$row->amount }}</td>
                    <td>{{$row->subject }}</td>
                    <td>{{$row->vat }}%</td>
                    <td class="right">&euro; {{ number_format(str_replace(',', '.', $row->price), 2, ',', '.') }}</td>
                    <td class="right">&euro; {{ number_format(((double)str_replace(',', '.', $row->price) * (int)$row->amount), 2, ',', '.') }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <br><br>

    <div class="price">
        <table width="100%">
            <tr>
                <td width="50%"></td>
                <td width="50%">
                    <table width="100%">
                        <tr>
                            <td width="40%"></td>
                            <td width="60%" class="priceBox">
                                <table width="100%">
                                    <tr>
                                        <td>Totaal</td>
                                        <td class="right">&euro; {{ number_format($data->getAmountExcludingVat(), 2, ',', '.') }}</td>
                                    </tr>

                                    @foreach($data->vatArray() as $row)
                                        <tr>
                                            <td>{{$row['label']}}</td>
                                            <td class="right">&euro; {{ number_format((double)$row['amount'], 2, ',', '.') }}</td>
                                        </tr>
                                    @endforeach

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%"></td>
                            <td width="60%" class="priceBox total">
                                <table width="100%">
                                    <tr>
                                        <td>Totaal inclusief</td>
                                        <td class="right">&euro; {{ number_format($data->getAmount(), 2, ',', '.') }}</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

</div>

<div class="footer">
    Betalingsvoorwaarden: binnen 14 dagen na factuurdatum.<br>
    De algemene leveringsvoorwaarden van de kamer van koophandel zijn van toepassing.
</div>
