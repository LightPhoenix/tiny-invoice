<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceRowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_row', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('amount')->default(1);
            $table->string('subject')->nullable();
            $table->string('price')->nullable();
            $table->integer('vat')->nullable();
            $table->integer('ordering')->nullable();
            $table->integer('invoice_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice');
    }
}
