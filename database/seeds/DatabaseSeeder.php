<?php

use Illuminate\Database\Seeder;
use \App\Models\Vat;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $oVat21 = new Vat();
        $oVat21->name = "21%";
        $oVat21->value = 21;
        $oVat21->save();

        $oVat6 = new Vat();
        $oVat6->name = "6%";
        $oVat6->value = 6;
        $oVat6->save();
    }
}
