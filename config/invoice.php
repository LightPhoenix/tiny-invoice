<?php

return [

    /* Allow user Registration */
    'register'          => false,

    /* Invoice information */
    'address'           => 'Rietzanger 16',
    'zip'               => '2396 JE',
    'city'              => 'Koudekerk a/d Rijn',
    'company_nr'        => 'KVK Nr. 27377105',
    'vat_nr'            => 'BTW Nr. NL 208761561 B01',
    'account_holder'    => 'Tnv. JCW Fioole',
    'account'           => 'NL 28 INGB 0002 0920 97',
    'email'             => 'info@futureprodesign.nl',
    'phone'             => '+31 643 935 999',
    'website'           => 'www.futureprodesign.nl'
];
