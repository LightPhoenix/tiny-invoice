<?php

namespace App\Http\Controllers;

use App\Models\InvoiceRow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class AjaxController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function buildRoutes() {

        $aMethods = get_class_methods($this);
        $aActionMethods = preg_grep("/^action_/", $aMethods);

        foreach ($aActionMethods as $row)
        {
            $sUrlName = substr($row, 7, (strlen($row) - 7));
            Route::post('/ajax/' . $sUrlName, ['uses' => 'AjaxController@' . $row]);

        }
    }


    public function action_saveValue(Request $request){
        $bSucces = false;

        if ($request->has('object') && $request->has('id') && $request->has('key') && $request->has('value')) {
            $oObject = 'App\\Models\\' . $request->input('object');
            $sKey = $request->input('key');
            $sValue = $request->input('value');
            $iId = $request->input('id');

            $renderedObject = $oObject::find($iId);

            if(is_double($sValue))
                $sValue = (double)$sValue;

            $renderedObject->$sKey = $sValue;
            $renderedObject->save();

            $bSucces = true;
        }


        return json_encode(['success' => $bSucces]);
    }

    public function action_deleteInvoiceRow(Request $request) {

        $bSucces = false;

        if ($request->has('invoice_row_id')) {
            $invoice_row_id = $request->input('invoice_row_id');

            $oInvoiceRow = InvoiceRow::find($invoice_row_id);
            $oInvoiceRow->delete();

            $bSucces = true;
        }

        return json_encode(['success' => $bSucces]);
    }

    public function action_createInvoiceRow(Request $request) {

        $bSucces = false;
        $iId = 0;

        if ($request->has('invoice_id')) {
            $invoice_id = $request->input('invoice_id');

            $oInvoiceRow = new InvoiceRow();
            $oInvoiceRow->invoice_id = $invoice_id;
            $oInvoiceRow->save();

            $bSucces = true;
            $iId = $oInvoiceRow->id;
        }

        return json_encode(['success' => $bSucces, 'id' => $iId]);
    }
}
