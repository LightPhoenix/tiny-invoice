<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $aData = Client::orderBy('companyname', 'asc')->paginate(25);
        $aParams = ['aData' => $aData];
        return view('client.index', $aParams);
    }

    public function addEdit($id = 0)
    {
        if($id == 0)
            $oClient = new Client();
        else
            $oClient = Client::find($id);

        $aParams = ['id' => $id, 'oClient' => $oClient, 'oController' => $this];
        return view('client.edit', $aParams);
    }

    public function deleteClient($id = 0)
    {
        $oClient = Client::find($id);
        $oClient->delete();

        return back()
            ->with('message-success', 'Klant #' . $id . ' verwijderd.');
    }

    public function addEditPost()
    {
        $oValidatior = $this->getClientValidator(Input::all());

        if($oValidatior->fails())
        {
            $failedRules = $oValidatior->failed();
            return back()
                ->with('message-error', 'Niet alle velden zijn correct ingevuld.')
                ->with('failedRules', $failedRules)
                ->withErrors($oValidatior)
                ->withInput();
        }
        else
        {
            $id = Input::get('id');
            $oClient = Client::firstOrNew(['id' => $id]);
            $oClient->fill(Input::all());
            $oClient->save();
            $messageSuccess = 'Nieuwe klant opgeslagen.';

            if($id > 0)
                $messageSuccess = 'Klant #' . $id . ' opgeslagen.';

            return redirect()
                ->route('client.index')
                ->with('message-success', $messageSuccess);
        }
    }

    protected function getClientValidator(array $data)
    {
        return Validator::make($data, [
            'companyname' => 'required|string',
            'email' => 'required|string|email|max:255|unique:client,email,' . $data['id'],
        ]);
    }

}
