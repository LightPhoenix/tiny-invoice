<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Models\Invoice;
use League\Flysystem\Config;

class PaymentController extends Controller
{

    private $oMollie;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->oMollie = new \Mollie_API_Client;
        $this->oMollie->setApiKey(config('ideal.api_key'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($hash = null)
    {
        $aIssuers = $this->oMollie->issuers->all();
        $oInvoice = Invoice::where('hash', '=', $hash)->first();

        $aSelect = [];

        foreach ($aIssuers as $row)
        {
            $aSelect[htmlspecialchars($row->id)] = htmlspecialchars($row->name);
        }

        return view('invoice.pay', ['hash' => $hash, 'oInvoice' => $oInvoice, 'aIssuers' => $aSelect]);
    }

    public function postPayment() {

        $oInvoice = Invoice::where('hash', '=', Input::get('hash'))->first();

        $payment = $this->oMollie->payments->create(array(
            "amount"      => doubleval($oInvoice->getAmount()),
            "description" => 'Factuur ' . date('Y', strtotime($oInvoice->date)) . '-' .  $oInvoice->id ,
            "redirectUrl" => route('pay.payed', $oInvoice->hash),
            "webhookUrl"  => route('pay.payed', $oInvoice->hash),
            "method"      => \Mollie_API_Object_Method::IDEAL,
            "issuer"      => Input::get('issuer_id'), // e.g. "ideal_INGBNL2A"
        ));

        $oInvoice->payment_id = $payment->id;
        $oInvoice->save();

        return redirect()->away($payment->getPaymentUrl());

    }

    public function payed($hash = null)
    {
        $oInvoice = Invoice::where('hash', '=', $hash)->first();
        $payment = $this->oMollie->payments->get($oInvoice->payment_id);

        $oInvoice->payment_created = date('Y-m-d H:i:s', strtotime($payment->createdDatetime));
        $oInvoice->payment_status = $payment->status;

        if($payment->status == 'paid') {
            $oInvoice->payed = date('Y-m-d H:i:s', strtotime(str_replace('T', '', str_replace('C', '', $payment->paidDatetime))));
        }

        $oInvoice->save();

        return $this->index($oInvoice->hash);
    }
}
