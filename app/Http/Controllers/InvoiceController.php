<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Vat;
use App\Models\Invoice;
use App\Models\InvoiceRow;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Barryvdh\DomPDF\Facade as PDF;

class InvoiceController extends Controller
{

    public function __construct()
    {
        /* Only allow PDF to be viewed without authentication (customers will be able to watch this) */
        $this->middleware('auth')->except('pdf');
    }

    public function index()
    {
        $aData = Invoice::orderBy('id', 'desc')->paginate(25);
        $aParams = ['aData' => $aData];
        return view('invoice.index', $aParams);
    }

    public function addEdit($id = 0)
    {
        if($id == 0)
            $oInvoice = new Invoice();
        else
            $oInvoice = Invoice::find($id);

        $aVatList = Vat::pluck('name', 'value')->toArray();
        $aVatList[null] = '';

        $aClientList = Client::orderBy('companyname','asc')->pluck('companyname', 'id');

        $aAmountList = [];

        $aInvoiceRows = [];

        if($id > 0)
            $aInvoiceRows = InvoiceRow::where('invoice_id', '=', $id)->get();

        for ($i = 1; $i < 1001; $i++)
            $aAmountList[$i] = $i;

        $aParams =
            [
                'id' => $id,
                'oInvoice' => $oInvoice,
                'oController' => $this,
                'aVatList' => $aVatList,
                'aClientList' => $aClientList,
                'aAmountList' => $aAmountList,
                'aInvoiceRows' => $aInvoiceRows
            ];
        return view('invoice.edit', $aParams);
    }

    public function deleteInvoice($id = 0)
    {
        $oClient = Invoice::find($id);
        $sInvoiceNr = date('Y', strtotime($oClient->date)) . '-' . $id;
        $oClient->delete();

        return back()
            ->with('message-success', 'Factuur #' . $sInvoiceNr . ' verwijderd.');
    }

    public function addEditPost()
    {
        $oValidatior = $this->getInvoiceValidator(Input::all());

        if($oValidatior->fails())
        {
            $failedRules = $oValidatior->failed();
            return back()
                ->with('message-error', 'Niet alle velden zijn correct ingevuld.')
                ->with('failedRules', $failedRules)
                ->withErrors($oValidatior)
                ->withInput();
        }
        else
        {
            $id = Input::get('id');
            $oInvoice = Invoice::firstOrNew(['id' => $id]);
            $oInvoice->fill(Input::all());

            if((int)Input::get('id') == 0)
                $oInvoice->hash = str_random(25); // required for payment

            if(Input::has('date'))
                $oInvoice->date = date('Y-m-d', strtotime(Input::get('date')));

            if(Input::has('date_due'))
                $oInvoice->date_due = date('Y-m-d', strtotime(Input::get('date_due')));

            if(Input::has('payed'))
                $oInvoice->payed = date('Y-m-d', strtotime(Input::get('payed')));

            $oInvoice->save();
            $messageSuccess = 'Factuur opgeslagen.';

            return redirect()
                ->route('invoice.edit', ['id' => $oInvoice->id])
                ->with('message-success', $messageSuccess);
        }
    }

    protected function getInvoiceValidator(array $data)
    {
        return Validator::make($data, [
            'subject' => 'required|string',
            'date' => 'required|date_format:d-m-Y',
            'date_due' => 'required|date_format:d-m-Y',
            'payed' => 'nullable|date_format:d-m-Y',
        ]);
    }

    public function pdf($hash)
    {
        $oInvoice = Invoice::where('hash', '=', $hash)->first();

        $pdf = PDF::loadView('invoice.pdf', ['data' => $oInvoice]);
        return $pdf->setPaper('a4')->stream("invoice_" . date('Y', strtotime($oInvoice->date)) . "-" . $oInvoice->id . ".pdf");
    }

}
