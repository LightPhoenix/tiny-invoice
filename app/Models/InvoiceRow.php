<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceRow extends Model
{
    protected $table = 'invoice_row';
}